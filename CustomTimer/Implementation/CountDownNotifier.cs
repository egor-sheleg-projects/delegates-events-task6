﻿using System;
using CustomTimer.Interfaces;

namespace CustomTimer.Implementation
{
    /// <inheritdoc/>
    public class CountDownNotifier : ICountDownNotifier
    {
        private readonly Timer timer;
        private Action<string, int> startHandler;
        private Action<string> stopHandler;
        private Action<string, int> tickHandler;

        public CountDownNotifier(Timer timer)
        {
            this.timer = timer;
        }

        /// <inheritdoc/>
        public void Init(Action<string, int> startHandler, Action<string> stopHandler, Action<string, int> tickHandler)
        {
            this.startHandler = startHandler;
            this.stopHandler = stopHandler;
            this.tickHandler = tickHandler;
        }

        /// <inheritdoc/>
        public void Run()
        {
            this.timer.Start(this.startHandler, this.tickHandler, this.stopHandler);
        }
    }
}
