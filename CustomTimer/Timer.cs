﻿using System.Threading;

namespace CustomTimer
{
    /// <summary>
    /// A custom class for simulating a countdown clock, which implements the ability to send a messages and additional
    /// information about the Started, Tick and Stopped events to any types that are subscribing the specified events.
    ///
    /// - When creating an object of Timer class, it must be assigned:
    ///     - name (not null or empty string, otherwise ArgumentException will be thrown);
    ///     - the number of ticks (the number must be greater than 0, otherwise an exception will throw an ArgumentException).
    ///
    /// - After the timer has been created, it can fire the Started event, the event should contain information about
    /// the name of the timer and the number of ticks to start.
    ///
    /// - After starting the timer, it fires Tick events, which contain information about the name of the timer and
    /// the number of ticks left for triggering, there should be delays between Tick events, delays are modeled by Thread.Sleep.
    ///
    /// - After all Tick events are triggered, the timer should start the Stopped event, the event should contain information about
    /// the name of the timer.
    /// </summary>
    public class Timer
    {
        private string name;
        private int numberOfTicks;

        public Timer(string name, int numberOfTicks)
        {
            this.Name = name;
            this.NumberOfTicks = numberOfTicks;
        }

        public event Action<string, int> TimerStarted;

        public event Action<string> TimerStopped;

        public event Action<string, int> TimerTick;

        public string Name
        {
            get => this.name;

            set
            {
                this.name = value;
                if (value == null || value.Length == 0)
                {
                    throw new ArgumentException("Name is empty or null.", nameof(this.Name));
                }
            }
        }

        public int NumberOfTicks
        {
            get => this.numberOfTicks;
            set
            {
                this.numberOfTicks = value;

                if (this.numberOfTicks < 0)
                {
                    throw new ArgumentException(nameof(this.numberOfTicks));
                }
            }
        }

        public void Start(Action<string, int> startDelegate, Action<string, int> tickDelegate, Action<string> stopDelegate)
        {
            this.TimerStarted = startDelegate;
            this.TimerTick = tickDelegate;
            this.TimerStopped = stopDelegate;
            this.OnStarted(this.TimerStarted);
        }

        public void OnStarted(Action<string, int> startDelegate)
        {
            this.TimerStarted = startDelegate;
            startDelegate?.Invoke(this.Name, this.NumberOfTicks);
            this.OnTick(this.TimerTick);
        }

        public void OnTick(Action<string, int> tickDelegate)
        {
            if (this.NumberOfTicks == 1)
            {
                this.OnStopped(this.TimerStopped);
            }
            else
            {
                this.TimerTick = tickDelegate;
                this.numberOfTicks--;
                tickDelegate?.Invoke(this.Name, this.NumberOfTicks);
                if (this.NumberOfTicks > 1)
                {
                    this.OnTick(tickDelegate);
                }
            }
        }

        public void OnStopped(Action<string> stopDelegate)
        {
            this.TimerStopped = stopDelegate;
            stopDelegate?.Invoke(this.Name);
        }
    }
}
